import * as bcrypt from "bcrypt";
import * as passport from "passport";
import * as passportLocal from "passport-local";

import Messages from "../constants/messages";

import { getRepository } from "typeorm";
import { Validator } from "class-validator";
import { User } from "../entities/User";
import { ExtractJwt, VerifiedCallback, Strategy } from "passport-jwt";

const _local_strategy = passportLocal.Strategy;
const _validator = new Validator();
const _user_repository = getRepository(User);

// ---------------------------------------------------
// LOCAL STRATEGY
// ---------------------------------------------------

passport.use(
	new _local_strategy(
		{
			usernameField: "email",
			passwordField: "password"
		},
		async (email: string, password: string, callback: any) => {
			const validationResult = await _validator.isEmail(email);

			if (!validationResult) {
				return callback(null, null, {
					message: Messages.invalid_email
				});
			}

			const user = await _user_repository.findOne({ email });

			if (!!user) {
				const passwordValidationResult = await bcrypt.compare(password, user.password);
				if (passwordValidationResult) {
					return callback(null, user, {
						message: Messages.successful_login
					});
				}
			}

			return callback(null, null, {
				message: Messages.incorrect_email_or_password
			});
		}
	)
);

// -----------------------------------
// BEARER-RULE
// -----------------------------------

passport.use(
	"bearer-rule",
	new Strategy(
		{
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			secretOrKey: process.env.CIPHER_KEY
		},
		async (jwtPayload: any, done: VerifiedCallback) => {
			const id = jwtPayload.id;
			const user = await _user_repository.findOne({ id });

			if (!!user) {
				const { id, role } = user;
				const authenticatedUser = new User({ id, role });

				return done(null, authenticatedUser);
			}

			return done(null, null);
		}
	)
);
