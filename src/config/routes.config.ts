import Paths from "../constants/paths";

import { UserController } from "../controllers/UserController";
import { AccountController } from "../controllers/AccountController";
import { ImagesController } from "../controllers/ImagesController";
import { EnquiriesController } from "../controllers/EnquiriesController";
import { PropertiesController } from "../controllers/PropertiesController";

export type RouteType<T, P extends keyof T> = {
	method: string;
	path: string;
	controller: T;
	action: P;
	restricted?: boolean;
	admin?: true;
};

type test = keyof ReturnType<() => UserController>;

const createRoute = <T, P extends keyof T>(
	method: "get" | "post" | "put" | "patch" | "delete",
	path: keyof typeof Paths,
	controller: T,
	action: string,
	restricted = false,
	admin = false
) => {
	return { method, path: Paths[path], controller, action, restricted, admin } as RouteType<T, P>;
};

const Routes = [
	createRoute("post", "account_authenticate", AccountController, "authenticate"),
	createRoute("post", "account_register", AccountController, "register"),
	createRoute("delete", "account_delete", AccountController, "delete"),

	createRoute("post", "images_upload", ImagesController, "upload"),
	createRoute("delete", "images_delete", ImagesController, "delete"),

	createRoute("post", "enquiries_create", EnquiriesController, "create"),
	createRoute("get", "enquiries_get_all", EnquiriesController, "get_all", true, true),
	createRoute("get", "enquiries_get_by_id", EnquiriesController, "get_by_id", true, true),
	createRoute("get", "enquiries_get_by_property", EnquiriesController, "get_all_by_property", true, true),

	createRoute("post", "properties_create", PropertiesController, "create", true, true),
	createRoute("get", "properties_get_all", PropertiesController, "get_all"),
	createRoute("get", "properties_get_by_id", PropertiesController, "get_by_id"),
	createRoute("patch", "properties_update", PropertiesController, "update"),
	createRoute("put", "properties_enable", PropertiesController, "enable"),
	createRoute("put", "properties_disable", PropertiesController, "disable"),
	createRoute("delete", "properties_delete", PropertiesController, "delete")
];

export default Routes;
