const Messages = {
	invalid_email: "Email address is not valid.",
	incorrect_email_or_password: "Incorrect email or password.",
	successful_login: "Logged in successfully.",
	existing_user: "A user with this email already exists.",
	unexpected_error: "An unexpected error occurred. Please try again in a few minutes.",
	image_upload_error: "An error occured while uploading the images. Please try again in a few minutes.",
	entity_not_found: "Entity was not found.",
	empty_property_in_enquiry: "An enquiry has to be attached to a property."
};

export default Messages;
