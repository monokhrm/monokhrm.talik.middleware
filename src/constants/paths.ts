const Paths = {
	account_authenticate: "/account/authenticate",
	account_register: "/account/register",
	account_delete: "/account/:id/delete",

	images_upload: "/images",
	images_delete: "/images/:id",

	enquiries_create: "/enquiries",
	enquiries_get_by_id: "/enquiries/:id",
	enquiries_get_all: "/enquiries",
	enquiries_get_by_property: "/enquiries/property/:property_id",

	properties_create: "/properties",
	properties_get_all: "/properties",
	properties_get_by_id: "/properties/:id",
	properties_update: "/properties/:id",
	properties_enable: "/properties/:id/enable",
	properties_disable: "/properties/:id/disable",
	properties_delete: "/properties/:id"
};

export default Paths;
