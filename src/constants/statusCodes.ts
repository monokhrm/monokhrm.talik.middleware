const StatusCodes = {
	not_found: 404,
	bad_request: 400,
	internal_server_error: 500
};

export default StatusCodes;
