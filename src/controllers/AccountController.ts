import * as passport from "passport";

import Messages from "../constants/messages";
import StatusCodes from "../constants/statusCodes";

import { ApiResponse } from "../dto/ApiResponse";
import { ApiResponseError } from "../dto/ApiResponseError";
import { IVerifyOptions } from "passport-local";
import { Inject } from "typedi";
import { Request, Response } from "express";
import { User } from "../entities/User";
import { UserService } from "../services/UserService";
import { UtilityService } from "../services/UtilityService";
import { Validator } from "class-validator";

export class AccountController {
	private readonly _user_service: UserService;
	private readonly _utility_service: UtilityService;
	private readonly _validator: Validator;

	public constructor(
		@Inject(type => UserService) user_service: UserService,
		@Inject(type => UtilityService) utility_service: UtilityService,
		@Inject(type => Validator) validator: Validator
	) {
		this._user_service = user_service;
		this._utility_service = utility_service;
		this._validator = validator;
	}

	async authenticate(req: Request, resp: Response) {
		const api_response = new ApiResponse();

		passport.authenticate("local", { session: false }, (error, user: User, info: IVerifyOptions) => {
			if (!user) {
				const { message } = info;

				api_response.errors = [new ApiResponseError({ message })];
				return resp.status(StatusCodes.bad_request).json(api_response);
			}

			req.login(user, { session: false }, error => {
				if (!!error) {
					api_response.errors = [new ApiResponseError({ message: error.toString() })];
					return resp.status(StatusCodes.bad_request).json(api_response);
				}

				const token = this._user_service.create_user_token(user);

				api_response.data = token;
				return resp.json(api_response);
			});
		})(req, resp);
	}

	async register(req: Request, resp: Response) {
		const user = new User(req.body);
		const api_response = new ApiResponse();
		const validation_result = await this._validator.validate(user);

		if (validation_result.length) {
			api_response.errors = this._utility_service.get_api_response_errors(validation_result);
			return api_response;
		}

		const existing_user = await this._user_service.find_user_by_email(user.email);

		if (!!existing_user) {
			api_response.errors = [new ApiResponseError({ message: Messages.existing_user })];
			return api_response;
		}

		try {
			const created_user = await this._user_service.create_user(user);
			const token = this._user_service.create_user_token(created_user);

			api_response.data = token;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}

	async delete(req: Request, resp: Response) {
		const id = this._utility_service.sanitize_string(req.params.id);
		const api_response = new ApiResponse();

		try {
			const deleted_user = await this._user_service.delete_user(id);

			if (!deleted_user) {
				return this._utility_service.handle_not_found_error(resp, api_response);
			}

			api_response.data = deleted_user;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}
}
