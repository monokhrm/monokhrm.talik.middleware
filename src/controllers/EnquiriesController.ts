import { ApiResponse } from "../dto/ApiResponse";
import { ApiResponseMeta } from "../dto/ApiResponseMeta";
import { ApiResponsePagination } from "../dto/ApiResponsePagination";
import { Enquiry } from "../entities/Enquiry";
import { EnquiryService } from "../services/EnquiryService";
import { Inject } from "typedi";
import { Request } from "express";
import { Response } from "express-serve-static-core";
import { UtilityService } from "../services/UtilityService";
import { Validator } from "class-validator";
import { EmailService } from "../services/EmailService";

export class EnquiriesController {
	private readonly _enquiry_service: EnquiryService;
	private readonly _utility_service: UtilityService;
	private readonly _email_service: EmailService;
	private readonly _validator: Validator;

	public constructor(
		@Inject(() => EnquiryService) enquiry_service: EnquiryService,
		@Inject(() => UtilityService) utility_service: UtilityService,
		@Inject(() => EmailService) email_service: EmailService,
		@Inject(() => Validator) validator: Validator
	) {
		this._enquiry_service = enquiry_service;
		this._utility_service = utility_service;
		this._email_service = email_service;
		this._validator = validator;
	}

	async create(req: Request, resp: Response) {
		const enquiry = new Enquiry(req.body);
		const api_response = new ApiResponse();
		const validation_result = await this._validator.validate(enquiry);

		if (validation_result.length) {
			api_response.errors = this._utility_service.get_api_response_errors(validation_result);
			return api_response;
		}

		try {
			const created_enquiry = await this._enquiry_service.create_enquiry(req.body);

			this._email_service.send_email({
				to: created_enquiry.email,
				subject: "Property enquiries",
				body: "Hello there, we have received your enquiry. Be rest assured that we'll get back to you as soon as possible. Best regards, Talik."
			});

			api_response.data = created_enquiry;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}

	async get_by_id(req: Request, resp: Response) {
		const api_response = new ApiResponse();
		const id = this._utility_service.sanitize_string(req.params.id);

		try {
			const enquiry = await this._enquiry_service.find_enquiry_by_id(id);

			if (!enquiry) {
				return this._utility_service.handle_not_found_error(resp, api_response);
			}

			api_response.data = enquiry;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}

	async get_all(req: Request, resp: Response) {
		const api_response = new ApiResponse();
		const api_response_meta = new ApiResponseMeta();
		const api_response_pagination = new ApiResponsePagination();
		const page = req.query.page ? parseInt(req.query.page) : 1;
		const page_size = parseInt(process.env.PAGE_SIZE);
		const order_direction = this._utility_service.normalize_order_direction(req.query.order);

		try {
			const enquiries = await this._enquiry_service.find_all(undefined, "created_at", order_direction);
			const paginated_enquires = this._utility_service.get_paginated_items(enquiries, page);

			api_response_pagination.total = enquiries.length;
			api_response_pagination.count = paginated_enquires.length;
			api_response_pagination.per_page = page_size;
			api_response_pagination.current_page = page;
			api_response_pagination.total_pages = Math.ceil(enquiries.length / page_size) || 1;

			api_response_meta.pagination = api_response_pagination;
			api_response.meta = api_response_meta;
			api_response.data = paginated_enquires;

			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}

	async get_all_by_property(req: Request, resp: Response) {
		const api_response = new ApiResponse();
		const api_response_meta = new ApiResponseMeta();
		const api_response_pagination = new ApiResponsePagination();
		const property_id = this._utility_service.sanitize_string(req.params.property_id);
		const page = req.query.page ? parseInt(req.query.page) : 1;
		const page_size = parseInt(process.env.PAGE_SIZE);
		const order_direction = this._utility_service.normalize_order_direction(req.query.order);

		try {
			const enquiries = await this._enquiry_service.find_all_by_property(property_id, undefined, undefined, order_direction);
			const paginated_enquires = this._utility_service.get_paginated_items(enquiries, page);

			api_response_pagination.total = enquiries.length;
			api_response_pagination.count = paginated_enquires.length;
			api_response_pagination.per_page = page_size;
			api_response_pagination.current_page = page;
			api_response_pagination.total_pages = Math.ceil(enquiries.length / page_size) || 1;

			api_response_meta.pagination = api_response_pagination;
			api_response.meta = api_response_meta;
			api_response.data = paginated_enquires;

			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}
}
