import Messages from "../constants/messages";
import StatusCodes from "../constants/statusCodes";

import { ApiResponse } from "../dto/ApiResponse";
import { ApiResponseError } from "../dto/ApiResponseError";
import { ImageService } from "../services/ImageService";
import { Inject } from "typedi";
import { Request, Response } from "express";
import { UtilityService } from "../services/UtilityService";

export class ImagesController {
	private readonly _image_service: ImageService;
	private readonly _utility_service: UtilityService;

	public constructor(@Inject(type => ImageService) image_service: ImageService, @Inject(type => UtilityService) utility_service: UtilityService) {
		this._image_service = image_service;
		this._utility_service = utility_service;
	}

	async upload(req: Request, resp: Response) {
		const api_response = new ApiResponse();

		try {
			const uploaded_images = await this._image_service.upload_images(req);

			if (uploaded_images.length === 0) {
				api_response.errors = [new ApiResponseError({ message: Messages.image_upload_error })];
				resp.status(StatusCodes.bad_request);
				return api_response;
			}

			api_response.data = uploaded_images;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}

	async delete(req: Request, resp: Response) {
		const id = this._utility_service.sanitize_string(req.params.id);
		const api_response = new ApiResponse();

		try {
			const deleted_image = await this._image_service.delete_image(id);

			if (!deleted_image) {
				return this._utility_service.handle_not_found_error(resp, api_response);
			}

			api_response.data = deleted_image;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}
}
