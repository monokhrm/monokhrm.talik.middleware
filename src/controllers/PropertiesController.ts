import { ApiResponse } from "../dto/ApiResponse";
import { ApiResponseMeta } from "../dto/ApiResponseMeta";
import { ApiResponsePagination } from "../dto/ApiResponsePagination";
import { Inject } from "typedi";
import { Property } from "../entities/Property";
import { PropertyRequestType } from "../typings/property";
import { PropertyService } from "../services/PropertyService";
import { Response, Request } from "express";
import { UtilityService } from "../services/UtilityService";
import { Validator } from "class-validator";
import { Like, Between, MoreThanOrEqual, LessThanOrEqual } from "typeorm";

export class PropertiesController {
	private readonly _property_service: PropertyService;
	private readonly _utility_service: UtilityService;
	private readonly _validator: Validator;

	public constructor(
		@Inject(type => PropertyService) property_service: PropertyService,
		@Inject(type => UtilityService) utility_service: UtilityService,
		@Inject(type => Validator) validator: Validator
	) {
		this._property_service = property_service;
		this._utility_service = utility_service;
		this._validator = validator;
	}

	async create(req: Request, resp: Response) {
		const property = new Property(req.body);
		const api_response = new ApiResponse();
		const validation_result = await this._validator.validate(property);

		if (validation_result.length) {
			api_response.errors = this._utility_service.get_api_response_errors(validation_result);
			return api_response;
		}

		try {
			const created_property = await this._property_service.create_property(property);
			api_response.data = created_property;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}

	async get_by_id(req: Request, resp: Response) {
		const api_response = new ApiResponse();
		const id = this._utility_service.sanitize_string(req.params.id);

		try {
			const created_property = await this._property_service.find_property_by_id(id);

			if (!created_property) {
				return this._utility_service.handle_not_found_error(resp, api_response);
			}

			api_response.data = created_property;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}

	async get_all(req: Request, resp: Response) {
		const api_response = new ApiResponse();
		const api_response_meta = new ApiResponseMeta();
		const api_response_pagination = new ApiResponsePagination();
		const filter_options = req.body as PropertyRequestType;
		const page = req.query.page ? parseInt(req.query.page) : 1;
		const page_size = parseInt(process.env.PAGE_SIZE);
		const order = this._utility_service.normalize_order(Property, req.params.sort_by);
		const order_direction = this._utility_service.normalize_order_direction(req.query.order);

		const filter = {};
		const { locality, type, bedrooms, min_price, max_price, furnished, tags } = filter_options;

		if (!this._utility_service.is_null_or_undefined(locality)) filter["address"] = { locality };
		if (!this._utility_service.is_null_or_undefined(type)) filter["type"] = type;
		if (!this._utility_service.is_null_or_undefined(bedrooms)) filter["bedrooms"] = bedrooms;
		if (!this._utility_service.is_null_or_undefined(furnished)) filter["furnished"] = furnished;
		if (!this._utility_service.is_null_or_undefined(tags)) filter["tags"] = Like(`%${tags}%`);

		if (!this._utility_service.is_null_or_undefined(min_price) && !this._utility_service.is_null_or_undefined(max_price)) {
			filter["price"] = Between(min_price, max_price);
		}

		if (!this._utility_service.is_null_or_undefined(min_price) && this._utility_service.is_null_or_undefined(max_price)) {
			filter["price"] = MoreThanOrEqual(min_price);
		}

		if (this._utility_service.is_null_or_undefined(min_price) && !this._utility_service.is_null_or_undefined(max_price)) {
			filter["price"] = LessThanOrEqual(max_price);
		}

		try {
			const properties = await this._property_service.find_all(filter, order, order_direction);
			const paginated_properties = this._utility_service.get_paginated_items(properties, page);

			api_response_pagination.total = properties.length;
			api_response_pagination.count = paginated_properties.length;
			api_response_pagination.per_page = page_size;
			api_response_pagination.current_page = page;
			api_response_pagination.total_pages = Math.ceil(properties.length / page_size) || 1;

			api_response_meta.pagination = api_response_pagination;
			api_response.meta = api_response_meta;
			api_response.data = paginated_properties;

			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}

	async update(req: Request, resp: Response) {
		const id = this._utility_service.sanitize_string(req.params.id);
		const property = new Property(req.body);
		const api_response = new ApiResponse();
		const validation_result = await this._validator.validate(property, { skipMissingProperties: true });

		if (validation_result.length) {
			api_response.errors = this._utility_service.get_api_response_errors(validation_result);
			return api_response;
		}

		try {
			const updated_property = await this._property_service.update_property(id, property);

			if (!updated_property) {
				return this._utility_service.handle_not_found_error(resp, api_response);
			}

			api_response.data = updated_property;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}

	async enable(req: Request, resp: Response) {
		const id = this._utility_service.sanitize_string(req.params.id);
		const api_response = new ApiResponse();

		try {
			const updated_property = await this._property_service.update_property(id, { is_disabled: false });

			if (!updated_property) {
				return this._utility_service.handle_not_found_error(resp, api_response);
			}

			api_response.data = updated_property;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}

	async disable(req: Request, resp: Response) {
		const id = this._utility_service.sanitize_string(req.params.id);
		const api_response = new ApiResponse();

		try {
			const updated_property = await this._property_service.update_property(id, { is_disabled: true });

			if (!updated_property) {
				return this._utility_service.handle_not_found_error(resp, api_response);
			}

			api_response.data = updated_property;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}

	async delete(req: Request, resp: Response) {
		const id = this._utility_service.sanitize_string(req.params.id);
		const api_response = new ApiResponse();

		try {
			const deleted_property = await this._property_service.delete_property(id);

			if (!deleted_property) {
				return this._utility_service.handle_not_found_error(resp, api_response);
			}

			api_response.data = deleted_property;
			return api_response;
		} catch (error) {
			return this._utility_service.handle_unknown_errors(resp, api_response, error);
		}
	}
}
