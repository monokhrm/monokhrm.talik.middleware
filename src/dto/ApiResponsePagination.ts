export class ApiResponsePagination {
	total: number;
	count: number;
	per_page: number;
	current_page: number;
	total_pages: number;

	constructor(dto: Partial<ApiResponsePagination> = {}) {
		this.total = dto.total;
		this.count = dto.count;
		this.per_page = dto.per_page;
		this.current_page = dto.current_page;
		this.total_pages = dto.total_pages;
	}
}
