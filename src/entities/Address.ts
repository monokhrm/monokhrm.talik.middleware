import { Entity, Column } from "typeorm";
import { BaseEntity } from "./BaseEntity";
import { IsNotEmpty } from "class-validator";

@Entity()
export class Address extends BaseEntity {
	@Column()
	@IsNotEmpty()
	address_line_1: string;

	@Column({
		nullable: true
	})
	address_line_2: string;

	@Column()
	@IsNotEmpty()
	locality: string;

	@Column()
	state: string;

	@Column()
	country: string;

	constructor(dto: Partial<Address> = {}) {
		super(dto);

		this.address_line_1 = dto.address_line_1;
		this.address_line_2 = dto.address_line_2;
		this.locality = dto.locality;
		this.state = dto.state;
		this.country = dto.country;
	}
}
