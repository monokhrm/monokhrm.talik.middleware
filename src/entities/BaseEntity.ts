import { PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";

export abstract class BaseEntity {
	@PrimaryGeneratedColumn("uuid")
	id: string;

	@CreateDateColumn()
	created_at: Date;

	@UpdateDateColumn()
	modified_at: Date;

	constructor(dto: Partial<BaseEntity> = {}) {
		this.id = dto.id;
		this.created_at = dto.created_at;
		this.modified_at = dto.modified_at;
	}
}
