import Messages from "../constants/messages";

import { Entity, Column } from "typeorm";
import { BaseEntity } from "./BaseEntity";
import { Matches, IsEmail, IsNotEmpty, MaxLength } from "class-validator";

@Entity()
export class Enquiry extends BaseEntity {
	@Column()
	@IsNotEmpty()
	@Matches(/^[a-zA-Z0-9\s]+$/)
	full_name: string;

	@Column()
	@IsNotEmpty()
	@IsEmail()
	email: string;

	@Column()
	@IsNotEmpty()
	phone_number: string;

	@Column({
		type: "longtext"
	})
	@MaxLength(500)
	message: string;

	@Column()
	@IsNotEmpty()
	reason: string;

	@Column()
	@IsNotEmpty({ message: Messages.empty_property_in_enquiry })
	property_id: string;

	constructor(dto: Partial<Enquiry> = {}) {
		super(dto);

		this.full_name = dto.full_name;
		this.email = dto.email;
		this.phone_number = dto.phone_number;
		this.message = dto.message;
		this.reason = dto.reason;
		this.property_id = dto.property_id;
	}
}
