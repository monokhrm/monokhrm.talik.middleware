import { BaseEntity } from "./BaseEntity";
import { Entity, Column } from "typeorm";

@Entity()
export class Image extends BaseEntity {
	@Column()
	width: number;

	@Column()
	height: number;

	@Column()
	uri: string;

	@Column()
	extension: string;

	@Column()
	size: number;

	@Column()
	image_path: string;

	constructor(dto: Partial<Image> = {}) {
		super(dto);

		this.width = dto.width;
		this.height = dto.height;
		this.uri = dto.uri;
		this.extension = dto.extension;
		this.size = dto.size;
		this.image_path = dto.image_path;
	}
}
