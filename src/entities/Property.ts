import { Address } from "./Address";
import { AreaUnitEnum } from "../enums/AreaUnitEnum";
import { BaseEntity } from "./BaseEntity";
import { Entity, Column, ManyToMany, JoinTable, OneToOne, JoinColumn, OneToMany } from "typeorm";
import { Image } from "./Image";
import { IsNotEmpty, MaxLength, Min, ValidateNested, IsNumber } from "class-validator";
import { PriceRateEnum } from "../enums/PriceRateEnum";
import { PropertyTypeEnum } from "../enums/PropertyTypeEnum";

@Entity()
export class Property extends BaseEntity {
	@Column({
		length: 100
	})
	@IsNotEmpty()
	@MaxLength(100)
	title: string;

	@Column({
		type: "longtext"
	})
	@IsNotEmpty()
	@MaxLength(2000)
	description: string;

	@OneToOne(type => Address, {
		cascade: true
	})
	@JoinColumn({
		name: "address_id",
		referencedColumnName: "id"
	})
	@IsNotEmpty()
	@ValidateNested()
	address: Address;

	@Column({
		type: "decimal",
		default: 0
	})
	@IsNotEmpty()
	@IsNumber()
	@Min(1)
	price: number;

	@Column()
	price_currency: string;

	@Column()
	@IsNotEmpty()
	rate: PriceRateEnum;

	@ManyToMany(() => Image, { eager: true })
	@JoinTable({
		name: "property_images_image",
		joinColumn: {
			name: "property_id",
			referencedColumnName: "id"
		},
		inverseJoinColumn: {
			name: "image_id",
			referencedColumnName: "id"
		}
	})
	images: Array<Image>;

	@ManyToMany(type => Image, { eager: true })
	@JoinTable({
		name: "property_floor_plans_image",
		joinColumn: {
			name: "property_id",
			referencedColumnName: "id"
		},
		inverseJoinColumn: {
			name: "image_id",
			referencedColumnName: "id"
		}
	})
	floor_plans: Array<Image>;

	@Column()
	@IsNotEmpty()
	type: PropertyTypeEnum;

	@Column()
	@IsNotEmpty()
	@IsNumber()
	@Min(1)
	area: number;

	@Column()
	@IsNotEmpty()
	area_unit: AreaUnitEnum;

	@Column()
	@IsNotEmpty()
	@IsNumber()
	@Min(1)
	floors: number;

	@Column()
	@IsNotEmpty()
	@IsNumber()
	bedrooms: number;

	@Column()
	@IsNotEmpty()
	@IsNumber()
	bathrooms: number;

	@Column()
	@IsNotEmpty()
	@IsNumber()
	parking_spaces: number;

	@Column()
	@IsNotEmpty()
	@IsNumber()
	kitchens: number;

	@Column()
	@IsNotEmpty()
	@IsNumber()
	toilets: number;

	@Column()
	@IsNotEmpty()
	date_available: Date;

	@Column({
		type: "longtext",
		nullable: true
	})
	nearby_landmarks: string;

	@Column({
		default: false
	})
	furnished: boolean;

	@Column({
		type: "longtext",
		nullable: true
	})
	features: string;

	@Column({
		type: "longtext",
		nullable: true
	})
	tags: string;

	@Column({
		default: false
	})
	is_disabled: boolean;

	constructor(dto: Partial<Property> = {}) {
		super(dto);

		this.title = dto.title;
		this.description = dto.description;
		this.address = dto.address ? new Address(dto.address) : null;
		this.price = dto.price;
		this.price_currency = dto.price_currency;
		this.rate = dto.rate;
		this.images = dto.images ? dto.images.map(o => new Image(o)) : null;
		this.floor_plans = dto.floor_plans ? dto.floor_plans.map(o => new Image(o)) : null;
		this.type = dto.type;
		this.area = dto.area;
		this.area_unit = dto.area_unit;
		this.floors = dto.floors;
		this.bedrooms = dto.bedrooms;
		this.bathrooms = dto.bathrooms;
		this.parking_spaces = dto.parking_spaces;
		this.kitchens = dto.kitchens;
		this.toilets = dto.toilets;
		this.date_available = dto.date_available;
		this.nearby_landmarks = dto.nearby_landmarks;
		this.furnished = dto.furnished;
		this.features = dto.features;
		this.tags = dto.tags;
		this.is_disabled = dto.is_disabled;
	}
}
