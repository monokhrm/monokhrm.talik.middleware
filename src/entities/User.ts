import { Entity, Column } from "typeorm";
import { BaseEntity } from "./BaseEntity";
import { UserRoleEnum } from "../enums/UserRoleEnum";
import { IsEmail, IsAlpha, MinLength } from "class-validator";

@Entity()
export class User extends BaseEntity {
	@Column()
	@IsAlpha()
	first_name: string;

	@Column()
	@IsAlpha()
	last_name: string;

	public get full_name(): string {
		return `${this.first_name} ${this.last_name}`;
	}

	@Column()
	role: UserRoleEnum;

	@Column({
		unique: true
	})
	@IsEmail()
	email: string;

	@Column()
	@MinLength(3, {
		message: "Password has to be a minimum of 3 characters"
	})
	password: string;

	constructor(dto: Partial<User> = {}) {
		super(dto);

		this.first_name = dto.first_name;
		this.last_name = dto.last_name;
		this.role = dto.role;
		this.email = dto.email;
		this.password = dto.password;
	}
}
