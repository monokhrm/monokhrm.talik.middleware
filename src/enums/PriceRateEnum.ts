export enum PriceRateEnum {
	Quarterly = "QUARTERLY",
	HalfYearly = "HALFYEARLY",
	Annually = "ANNUALLY",
	BiAnnually = "BIANNUALLY"
}
