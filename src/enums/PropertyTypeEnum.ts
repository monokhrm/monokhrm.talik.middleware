export enum PropertyTypeEnum {
	Flat = "FLAT",
	MiniFlat = "MINI FLAT",
	SelfContain = "SELF CONTAIN",
	SemiDetachedBungalow = "SEMI DETACHED BUNGALOW",
	SemiDetachedDuplex = "SEMI DETACHED DUPLEX",
	DetachedBungalow = "DETACHED BUNGALOW",
	DetachedDuplex = "DETACHED DUPLEX",
	TerracedBungalow = "TERRACED BUNGALOW",
	TerracedDuplex = "TERRACED DUPLEX",
	Warehouse = "WAREHOUSE",
	OfficeSpace = "OFFICE SPACE"
}
