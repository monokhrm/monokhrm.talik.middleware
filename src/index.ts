import "reflect-metadata";

import * as bodyParser from "body-parser";
import * as express from "express";
import * as fileUpload from "express-fileupload";
import * as passport from "passport";

import Container from "typedi";
import Routes, { RouteType } from "./config/routes.config";

import { createConnection, useContainer } from "typeorm";
import { Request, Response } from "express";
import { UserRoleEnum } from "./enums/UserRoleEnum";

// ----------------------------------------------------------------------
// CONFIGURE D.I. CONTAINER
// ----------------------------------------------------------------------

useContainer(Container);

// ----------------------------------------------------------------------
// CREATE ORM CONNECTION
// ----------------------------------------------------------------------

createConnection()
	.then(async connection => {
		// ----------------------------------------------------------------------
		// AUTHENTICATION CONFIG
		// ----------------------------------------------------------------------

		require("./config/authentication.config");

		const app = express();

		app.use(bodyParser.json());

		// ----------------------------------------------------------------------
		// FILE UPLOAD CONFIG
		// ----------------------------------------------------------------------

		app.use(
			fileUpload({
				limits: { fileSize: 5 * 1024 * 1024 }, //== SET THE MAXIMUM UPLOAD SIZE TO 5MB ==//
				safeFileNames: true,
				preserveExtension: 4,
				abortOnLimit: true
			})
		);

		// ----------------------------------------------------------------------
		// ROUTING
		// ----------------------------------------------------------------------

		Routes.forEach((route: RouteType<any, any>) => {
			const { path, method, controller, action, restricted, admin } = route;

			if (!!restricted) {
				app[method](path, passport.authenticate("bearer-rule", { session: false }), (req: Request, res: Response, next: Function) => {
					const role = req.user.role;

					// ----------------------------------------------------------------------
					// CHECK THAT USER IS ADMIN ROLE, IF NEEDED
					// ----------------------------------------------------------------------

					if (!!admin && role !== UserRoleEnum.Admin) {
						res.status(401).send();
						return;
					}

					// ----------------------------------------------------------------------
					// ROUTE USER TO APPROPRIATE CONTROLLER
					// ----------------------------------------------------------------------

					const result = Container.get(controller)[action](req, res, next);

					if (result instanceof Promise) {
						result.then(result => (result !== null && result !== undefined ? res.send(result) : undefined));
						return;
					}

					if (result !== null && result != undefined) {
						res.json(result);
						return;
					}
				});
			} else {
				app[method](path, (req: Request, res: Response, next: Function) => {
					const result = Container.get(controller)[action](req, res, next);

					if (result instanceof Promise) {
						result.then(result => (result !== null && result !== undefined ? res.send(result) : undefined));
						return;
					}

					if (result !== null && result != undefined) {
						res.json(result);
						return;
					}
				});
			}
		});

		const port = process.env.PORT;

		app.listen(port);

		console.log("Express server has started on port %s. Open http://localhost:%s/users to see results", port, port);
	})
	.catch(error => console.log(error));
