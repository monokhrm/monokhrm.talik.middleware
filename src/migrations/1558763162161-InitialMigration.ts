import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1558763162161 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `address` (`id` varchar(36) NOT NULL, `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `modified_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `address_line_1` varchar(255) NOT NULL, `address_line_2` varchar(255) NOT NULL, `locality` varchar(255) NOT NULL, `state` varchar(255) NOT NULL, `country` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `enquiry` (`id` varchar(36) NOT NULL, `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `modified_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `full_name` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `phone_number` varchar(255) NOT NULL, `message` longtext NOT NULL, `reason` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `image` (`id` varchar(36) NOT NULL, `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `modified_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `width` int NOT NULL, `height` int NOT NULL, `uri` varchar(255) NOT NULL, `extension` varchar(255) NOT NULL, `size` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `property` (`id` varchar(36) NOT NULL, `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `modified_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `title` varchar(100) NOT NULL, `description` longtext NOT NULL, `price` decimal NOT NULL, `price_currency` varchar(255) NOT NULL, `rate` varchar(255) NOT NULL, `type` varchar(255) NOT NULL, `area` int NOT NULL, `area_unit` varchar(255) NOT NULL, `floors` int NOT NULL, `bedrooms` int NOT NULL, `bathrooms` int NOT NULL, `parking_spaces` int NOT NULL, `kitchens` int NOT NULL, `toilets` int NOT NULL, `date_available` datetime NOT NULL, `nearby_landmarks` longtext NULL, `furnished` tinyint NOT NULL DEFAULT 0, `features` longtext NULL, `tags` longtext NULL, `address_id` varchar(36) NULL, UNIQUE INDEX `REL_d3a7ce3f61b608f57e46fb2921` (`address_id`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user` (`id` varchar(36) NOT NULL, `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `modified_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `first_name` varchar(255) NOT NULL, `last_name` varchar(255) NOT NULL, `role` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, UNIQUE INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `property_images_image` (`property_id` varchar(36) NOT NULL, `image_id` varchar(36) NOT NULL, INDEX `IDX_1cf7ca401ff62624638a4430df` (`property_id`), INDEX `IDX_4d825e8df05eea75e48948fb23` (`image_id`), PRIMARY KEY (`property_id`, `image_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `property_floor_plans_image` (`property_id` varchar(36) NOT NULL, `image_id` varchar(36) NOT NULL, INDEX `IDX_35359f5c471e27be158dfa42c0` (`property_id`), INDEX `IDX_e121423098b345c1ab65a41913` (`image_id`), PRIMARY KEY (`property_id`, `image_id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `property` ADD CONSTRAINT `FK_d3a7ce3f61b608f57e46fb2921c` FOREIGN KEY (`address_id`) REFERENCES `address`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `property_images_image` ADD CONSTRAINT `FK_1cf7ca401ff62624638a4430df7` FOREIGN KEY (`property_id`) REFERENCES `property`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `property_images_image` ADD CONSTRAINT `FK_4d825e8df05eea75e48948fb23e` FOREIGN KEY (`image_id`) REFERENCES `image`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `property_floor_plans_image` ADD CONSTRAINT `FK_35359f5c471e27be158dfa42c00` FOREIGN KEY (`property_id`) REFERENCES `property`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `property_floor_plans_image` ADD CONSTRAINT `FK_e121423098b345c1ab65a419135` FOREIGN KEY (`image_id`) REFERENCES `image`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `property_floor_plans_image` DROP FOREIGN KEY `FK_e121423098b345c1ab65a419135`");
        await queryRunner.query("ALTER TABLE `property_floor_plans_image` DROP FOREIGN KEY `FK_35359f5c471e27be158dfa42c00`");
        await queryRunner.query("ALTER TABLE `property_images_image` DROP FOREIGN KEY `FK_4d825e8df05eea75e48948fb23e`");
        await queryRunner.query("ALTER TABLE `property_images_image` DROP FOREIGN KEY `FK_1cf7ca401ff62624638a4430df7`");
        await queryRunner.query("ALTER TABLE `property` DROP FOREIGN KEY `FK_d3a7ce3f61b608f57e46fb2921c`");
        await queryRunner.query("DROP INDEX `IDX_e121423098b345c1ab65a41913` ON `property_floor_plans_image`");
        await queryRunner.query("DROP INDEX `IDX_35359f5c471e27be158dfa42c0` ON `property_floor_plans_image`");
        await queryRunner.query("DROP TABLE `property_floor_plans_image`");
        await queryRunner.query("DROP INDEX `IDX_4d825e8df05eea75e48948fb23` ON `property_images_image`");
        await queryRunner.query("DROP INDEX `IDX_1cf7ca401ff62624638a4430df` ON `property_images_image`");
        await queryRunner.query("DROP TABLE `property_images_image`");
        await queryRunner.query("DROP INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
        await queryRunner.query("DROP INDEX `REL_d3a7ce3f61b608f57e46fb2921` ON `property`");
        await queryRunner.query("DROP TABLE `property`");
        await queryRunner.query("DROP TABLE `image`");
        await queryRunner.query("DROP TABLE `enquiry`");
        await queryRunner.query("DROP TABLE `address`");
    }

}
