import {MigrationInterface, QueryRunner} from "typeorm";

export class ImageAddImagePath1558988605969 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `image` ADD `image_path` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `property` CHANGE `price` `price` decimal NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `property` CHANGE `price` `price` decimal(10,0) NOT NULL");
        await queryRunner.query("ALTER TABLE `image` DROP COLUMN `image_path`");
    }

}
