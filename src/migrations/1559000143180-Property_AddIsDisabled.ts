import {MigrationInterface, QueryRunner} from "typeorm";

export class PropertyAddIsDisabled1559000143180 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `enquiry` ADD `property_id` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `property` ADD `is_disabled` tinyint NOT NULL DEFAULT 0");
        await queryRunner.query("ALTER TABLE `property` CHANGE `price` `price` decimal NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `property` CHANGE `price` `price` decimal(10,0) NOT NULL");
        await queryRunner.query("ALTER TABLE `property` DROP COLUMN `is_disabled`");
        await queryRunner.query("ALTER TABLE `enquiry` DROP COLUMN `property_id`");
    }

}
