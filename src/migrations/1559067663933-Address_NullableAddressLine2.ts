import {MigrationInterface, QueryRunner} from "typeorm";

export class AddressNullableAddressLine21559067663933 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `address` CHANGE `address_line_2` `address_line_2` varchar(255) NULL");
        await queryRunner.query("ALTER TABLE `property` CHANGE `price` `price` decimal NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `property` CHANGE `price` `price` decimal(10,0) NOT NULL");
        await queryRunner.query("ALTER TABLE `address` CHANGE `address_line_2` `address_line_2` varchar(255) NOT NULL");
    }

}
