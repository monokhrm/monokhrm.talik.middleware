import {MigrationInterface, QueryRunner} from "typeorm";

export class PropertyPriceDefaultValue1559067742220 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `property` CHANGE `price` `price` decimal NOT NULL DEFAULT 0");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `property` CHANGE `price` `price` decimal(10,0) NOT NULL");
    }

}
