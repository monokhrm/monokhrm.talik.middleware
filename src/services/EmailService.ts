import * as Nodemailer from "nodemailer";

import { Service, Inject } from "typedi";
import { UtilityService } from "./UtilityService";
import { SendEmailConfigType } from "../typings/email";
import { LogService } from "./LogService";

@Service()
export class EmailService {
	private readonly _utility_service: UtilityService;
	private readonly _log_service: LogService;

	public constructor(@Inject(type => UtilityService) utility_service: UtilityService, @Inject(type => LogService) log_service: LogService) {
		this._utility_service = utility_service;
		this._log_service = log_service;
	}

	async send_email(config: SendEmailConfigType) {
		try {
			const transporter = await Nodemailer.createTransport({
				host: process.env.SMTP_HOST,
				port: parseInt(process.env.SMTP_PORT),
				secure: process.env.SMTP_SECURE == "true",
				auth: {
					user: process.env.SMTP_AUTH_USERNAME,
					pass: process.env.SMTP_AUTH_PASSWORD
				}
			} as any);
			const { to, subject, body: html, attachments } = config;
			const send_mail_options = {
				from: {
					name: "Talik Investment Ltd.",
					address: process.env.SMTP_SENDER
				},
				to,
				subject,
				html,
				attachments
			} as Nodemailer.SendMailOptions;

			await transporter.sendMail(send_mail_options);
		} catch (error) {
			this._log_service.log_exception(error);
		}
	}
}
