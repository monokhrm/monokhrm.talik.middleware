import { Enquiry } from "../entities/Enquiry";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { Service, Inject } from "typedi";
import { UtilityService } from "./UtilityService";
import { PropertyService } from "./PropertyService";

@Service()
export class EnquiryService {
	private readonly _enquiry_repository: Repository<Enquiry>;
	private readonly _property_service: PropertyService;
	private readonly _utility_service: UtilityService;

	public constructor(
		@InjectRepository(Enquiry) enquiry_repository: Repository<Enquiry>,
		@Inject(type => PropertyService) property_service: PropertyService,
		@Inject(type => UtilityService) utility_service: UtilityService
	) {
		this._enquiry_repository = enquiry_repository;
		this._utility_service = utility_service;
		this._property_service = property_service;
	}

	async create_enquiry(enquiry: Enquiry): Promise<Enquiry> {
		const { full_name, email, phone_number, message, reason, property_id } = enquiry;
		const enquiry_to_create = new Enquiry({
			full_name: this._utility_service.to_title_case(full_name),
			email: email.toLowerCase(),
			phone_number,
			message: this._utility_service.sanitize_string(message),
			reason,
			property_id
		});

		try {
			const property = await this._property_service.find_property_by_id(property_id);

			if (!property) {
				return Promise.resolve(undefined);
			}

			const created_enquiry = await this._enquiry_repository.save(enquiry_to_create);

			return Promise.resolve(created_enquiry);
		} catch (error) {
			return Promise.reject(error);
		}
	}

	async find_enquiry_by_id(id: string): Promise<Enquiry> {
		const enquiry = await this._enquiry_repository.findOne({ id });
		return Promise.resolve(enquiry);
	}

	async find_all(
		where?: Array<Partial<Enquiry>> | Partial<Enquiry>,
		order_by: keyof Enquiry = "created_at",
		order_direction: "ASC" | "DESC" = "DESC"
	): Promise<Array<Enquiry>> {
		const enquiries = await this._enquiry_repository.find({
			where,
			order: {
				[order_by]: order_direction
			}
		});
		return Promise.resolve(enquiries);
	}

	async find_all_by_property(
		property_id: string,
		where?: Array<Partial<Enquiry>> | Partial<Enquiry>,
		order_by: keyof Enquiry = "created_at",
		order_direction: "ASC" | "DESC" = "DESC"
	): Promise<Array<Enquiry>> {
		where = !!where ? (Array.isArray(where) ? [...where, { property_id }] : [where, { property_id }]) : { property_id };
		const enquiries = await this._enquiry_repository.find({
			where,
			order: {
				[order_by]: order_direction
			}
		});
		return Promise.resolve(enquiries);
	}
}
