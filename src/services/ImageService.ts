import * as ImageKit from "imagekit";
import * as fileUpload from "express-fileupload";

import { Image } from "../entities/Image";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { Request } from "express";
import { Service, Inject } from "typedi";
import { UploadedFileType } from "../typings/imagekit";
import { UtilityService } from "./UtilityService";

@Service()
export class ImageService {
	private readonly _image_repository: Repository<Image>;
	private readonly _utility_service: UtilityService;
	private readonly _image_kit: ImageKit;

	public constructor(
		@InjectRepository(Image) image_repository: Repository<Image>,
		@Inject(type => UtilityService) utility_service: UtilityService,
		image_kit = new ImageKit({
			imagekitId: process.env.IMAGEKIT_ID,
			apiKey: process.env.IMAGEKIT_PUBLIC_KEY,
			apiSecret: process.env.IMAGEKIT_PRIVATE_KEY,
			useSecure: true
		})
	) {
		this._image_repository = image_repository;
		this._utility_service = utility_service;
		this._image_kit = image_kit;
	}

	async upload_images(req: Request): Promise<Array<Image>> {
		if (!req.files) return Promise.resolve(new Array<Image>());

		const uploaded_images = new Array<Image>();
		const files = req.files;
		const files_keys = Object.keys(req.files);

		for (let file_key of files_keys) {
			const file = files[file_key] as fileUpload.UploadedFile;
			const extension = this._utility_service.get_file_extension(file_key);
			const base_64_string = file.data.toString("base64");

			try {
				const uploaded_file = await this.upload_to_cdn(base_64_string);
				const { width, height, url: uri, size, imagePath: image_path } = uploaded_file;
				const image_to_create = new Image({ width, height, uri, size, extension, image_path });
				const created_image = await this.create_image(image_to_create);
				uploaded_images.push(created_image);
			} catch (error) {
				continue;
			}
		}

		return Promise.resolve(uploaded_images);
	}

	async upload_to_cdn(base_64_string: string): Promise<UploadedFileType> {
		const file_name = this._utility_service.generate_random_string(15);
		try {
			const uploaded_image = await this._image_kit.upload(base_64_string, {
				filename: file_name,
				folder: process.env.IMAGEKIT_IMAGE_UPLOAD_FOLDER
			});
			return Promise.resolve(uploaded_image);
		} catch (error) {
			return Promise.reject(error);
		}
	}

	async remove_from_cdn(image_path: string): Promise<boolean> {
		try {
			await this._image_kit.deleteFile(image_path);
		} catch (error) {
			return Promise.reject(false);
		}
	}

	async create_image(image: Image): Promise<Image> {
		try {
			const created_image = await this._image_repository.save(image);
			const { id, created_at } = created_image;

			image.id = id;
			image.created_at = created_at;

			return Promise.resolve(image);
		} catch (error) {
			return Promise.reject(error);
		}
	}

	async find_image_by_id(id: string): Promise<Image> {
		const image = await this._image_repository.findOne({ id });
		return Promise.resolve(image);
	}

	async delete_image(id: string): Promise<Image> {
		try {
			const image_to_delete = await this.find_image_by_id(id);

			if (!image_to_delete) {
				return Promise.resolve(undefined);
			}

			await this.remove_from_cdn(image_to_delete.image_path);
			await this._image_repository.delete({ id });

			return Promise.resolve(image_to_delete);
		} catch (error) {
			return Promise.reject(error);
		}
	}
}
