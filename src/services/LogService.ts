import { Service } from "typedi";

@Service()
export class LogService {
	// -------------------------------------------------------------------------------------------------
	/** Logs an exception */
	log_exception(error: Error): void {
		console.log("error", error);
	}
}
