import { Address } from "../entities/Address";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Property } from "../entities/Property";
import { Repository } from "typeorm";
import { Service, Inject } from "typedi";
import { UtilityService } from "./UtilityService";

@Service()
export class PropertyService {
	private readonly _property_repository: Repository<Property>;
	private readonly _address_repository: Repository<Address>;
	private readonly _utility_service: UtilityService;

	public constructor(
		@InjectRepository(Property) property_repository: Repository<Property>,
		@InjectRepository(Address) address_repository: Repository<Address>,
		@Inject(type => UtilityService) utility_service: UtilityService
	) {
		this._property_repository = property_repository;
		this._address_repository = address_repository;
		this._utility_service = utility_service;
	}

	async create_property(property: Property): Promise<Property> {
		const {
			title,
			description,
			address,
			price,
			price_currency,
			rate,
			images,
			floor_plans,
			type,
			area,
			area_unit,
			floors,
			bedrooms,
			bathrooms,
			parking_spaces,
			kitchens,
			toilets,
			date_available,
			nearby_landmarks,
			furnished,
			features,
			tags
		} = property;
		const { address_line_1, address_line_2, locality } = address;
		const property_to_create = new Property({
			title: this._utility_service.to_title_case(title),
			description: this._utility_service.sanitize_string(description),
			address: new Address({ address_line_1, address_line_2, locality, state: "FCT", country: "Nigeria" }),
			price,
			price_currency,
			rate,
			images,
			floor_plans,
			type,
			area,
			area_unit,
			floors,
			bedrooms,
			bathrooms,
			parking_spaces,
			kitchens,
			toilets,
			date_available,
			nearby_landmarks: JSON.stringify(nearby_landmarks),
			furnished,
			features: JSON.stringify(features),
			tags: JSON.stringify(tags)
		});

		try {
			const created_property = await this._property_repository.save(property_to_create);
			return Promise.resolve(created_property);
		} catch (error) {
			return Promise.reject(error);
		}
	}

	async find_property_by_id(id: string): Promise<Property> {
		const property = await this._property_repository.findOne({
			where: { id },
			relations: ["address"]
		});

		return Promise.resolve(property);
	}

	async find_all(
		where?: Array<Partial<Property>> | Partial<Property>,
		order_by: keyof Property = "created_at",
		order_direction: "ASC" | "DESC" = "DESC"
	): Promise<Array<Property>> {
		console.log("here", where);
		const properties = await this._property_repository.find({
			where,
			relations: ["address"],
			order: {
				[order_by]: order_direction
			}
		});
		return Promise.resolve(properties);
	}

	async update_property(id: string, property: Partial<Property>): Promise<Property> {
		const pre_updated_property = await this.find_property_by_id(id);

		if (!pre_updated_property) {
			return Promise.resolve(undefined);
		}

		const {
			title,
			description,
			address,
			price,
			price_currency,
			rate,
			images,
			floor_plans,
			type,
			area,
			area_unit,
			floors,
			bedrooms,
			bathrooms,
			parking_spaces,
			kitchens,
			toilets,
			date_available,
			nearby_landmarks,
			furnished,
			features,
			tags,
			is_disabled
		} = property;
		let address_to_update = address;
		let property_to_update = new Property({
			title: this._utility_service.to_title_case(title),
			description: this._utility_service.sanitize_string(description),
			price,
			price_currency,
			rate,
			images,
			floor_plans,
			type,
			area,
			area_unit,
			floors,
			bedrooms,
			bathrooms,
			parking_spaces,
			kitchens,
			toilets,
			date_available,
			nearby_landmarks: JSON.stringify(nearby_landmarks),
			furnished,
			features: JSON.stringify(features),
			tags: JSON.stringify(tags),
			is_disabled
		});

		address_to_update = this._utility_service.purge_object(address_to_update);
		property_to_update = this._utility_service.purge_object(property_to_update);

		try {
			if (!!address_to_update) {
				await this._address_repository.update(pre_updated_property.address.id, address_to_update);
			}

			await this._property_repository.update(id, property_to_update);

			const updated_address = Object.assign(pre_updated_property.address, address_to_update);
			const updated_property = Object.assign(pre_updated_property, property_to_update);

			updated_property.address = updated_address;

			return Promise.resolve(updated_property);
		} catch (error) {
			return Promise.reject(error);
		}
	}

	async delete_property(id: string): Promise<Property> {
		try {
			const property_to_delete = await this.find_property_by_id(id);

			if (!property_to_delete) {
				return Promise.resolve(undefined);
			}

			await this._property_repository.delete({ id });
			return Promise.resolve(property_to_delete);
		} catch (error) {
			return Promise.reject(error);
		}
	}
}
