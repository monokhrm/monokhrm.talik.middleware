import * as JWT from "jsonwebtoken";

import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { Request } from "express";
import { Service, Inject } from "typedi";
import { User } from "../entities/User";
import { UserRoleEnum } from "../enums/UserRoleEnum";
import { UtilityService } from "./UtilityService";

@Service()
export class UserService {
	private readonly _user_repository: Repository<User>;
	private readonly _utility_service: UtilityService;

	public constructor(@InjectRepository(User) user_repository: Repository<User>, @Inject(type => UtilityService) utility_service: UtilityService) {
		this._user_repository = user_repository;
		this._utility_service = utility_service;
	}

	async create_user(user: User): Promise<User> {
		const { first_name, last_name, email, password } = user;
		const user_to_create = new User({ first_name, last_name, email: email.toLowerCase() });

		user_to_create.password = await this._utility_service.get_hash(password);
		user_to_create.role = UserRoleEnum.Admin;

		try {
			const created_user = await this._user_repository.save(user_to_create);
			const { id, created_at } = created_user;

			user.id = id;
			user.created_at = created_at;

			return Promise.resolve(user);
		} catch (error) {
			return Promise.reject(error);
		}
	}

	create_user_token(user: User): string {
		const { id, email } = user;
		return JWT.sign({ id, email }, process.env.CIPHER_KEY, { expiresIn: "7 days" });
	}

	async find_user_by_email(email: string, clean_password = true): Promise<User> {
		const user = await this._user_repository.findOne({ email });
		if (!!user && clean_password) delete user.password;
		return Promise.resolve(user);
	}

	async find_user_by_id(id: string, clean_password = true): Promise<User> {
		const user = await this._user_repository.findOne({ id });
		if (!!user && clean_password) delete user.password;
		return Promise.resolve(user);
	}

	async find_all(where?: Array<Partial<User>> | Partial<User>, order_by: keyof User = "first_name", order_direction: "ASC" | "DESC" = "ASC"): Promise<Array<User>> {
		const users = await this._user_repository.find({
			where,
			order: {
				[order_by]: order_direction
			}
		});
		return Promise.resolve(users);
	}

	async update_user(id: string, user: Partial<User>): Promise<User> {
		const { password } = user;

		if (!!password) {
			user.password = await this._utility_service.get_hash(password);
		}

		user = this._utility_service.purge_object(user);

		try {
			await this._user_repository.update(id, user);
			const updated_user = await this.find_user_by_id(id);

			return Promise.resolve(updated_user);
		} catch (error) {
			return Promise.reject(error);
		}
	}

	async delete_user(id: string): Promise<User> {
		try {
			const user_to_delete = await this.find_user_by_id(id);

			if (!user_to_delete) {
				return Promise.resolve(undefined);
			}

			await this._user_repository.delete({ id });

			return Promise.resolve(user_to_delete);
		} catch (error) {
			return Promise.reject(error);
		}
	}

	get_authenticated_user_id(request: Request): string {
		const authenticated_user = request.user as User;
		return authenticated_user.id;
	}

	get_authenticated_user_role(request: Request): UserRoleEnum {
		const authenticated_user = request.user as User;
		return authenticated_user.role;
	}

	async get_authenticated_user(request: Request, clean_password = true): Promise<User> {
		const id = this.get_authenticated_user_id(request);

		try {
			const user = await this.find_user_by_id(id, clean_password);
			return Promise.resolve(user);
		} catch (error) {
			return Promise.reject(error);
		}
	}
}
