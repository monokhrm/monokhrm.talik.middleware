import * as bcrypt from "bcrypt";
import * as Chance from "chance";

import Messages from "../constants/messages";
import StatusCodes from "../constants/statusCodes";

import { ApiResponse } from "../dto/ApiResponse";
import { ApiResponseError } from "../dto/ApiResponseError";
import { LogService } from "./LogService";
import { Service, Inject } from "typedi";
import { ValidationError } from "class-validator";
import { filterXSS } from "xss";
import { promisify } from "util";
import { readFile } from "fs";
import { Response } from "express";

@Service()
export class UtilityService {
	private readonly _log_service: LogService;
	private readonly _chance: Chance.Chance;

	public constructor(chance = new Chance(), @Inject(type => LogService) log_service: LogService) {
		this._chance = chance;
		this._log_service = log_service;
	}

	// -------------------------------------------------------------------------------------------------
	/** Gets api response errors from validation errors including the nested validation errors */
	get_api_response_errors(validationResult: Array<ValidationError>): Array<ApiResponseError> {
		const apiResponseErrors = new Array<ApiResponseError>();

		const validationErrors = validationResult.filter(x => !!x.constraints);
		const nestedValidationErrors = validationResult.filter(x => x.children.length).reduce((a, b) => [...a, ...b.children], new Array<ValidationError>());
		const aggregateValidationErrors = [...validationErrors, ...nestedValidationErrors];

		aggregateValidationErrors.forEach(o => {
			for (var key in o.constraints) {
				const apiResponseError = new ApiResponseError({
					property: o.property,
					message: this.to_sentence_case(o.constraints[key].replace(o.property, o.property.split("_").join(" ")))
				});
				apiResponseErrors.push(apiResponseError);
			}
		});

		return apiResponseErrors;
	}

	// -------------------------------------------------------------------------------------------------
	/** Returns the hash of given data */
	async get_hash(data: any): Promise<string> {
		return bcrypt.hash(data, 2);
	}

	// -------------------------------------------------------------------------------------------------
	/** Converts provided text to Sentence case */
	to_sentence_case(source: string): string {
		if (this.is_null_or_undefined(source)) return undefined;
		return `${source[0].toUpperCase()}${source.slice(1).toLowerCase()}`;
	}

	// -------------------------------------------------------------------------------------------------
	/** Converts provided text to Title case */
	to_title_case(source: string): string {
		if (this.is_null_or_undefined(source)) return undefined;
		const exceptions = ["in", "the", "a", "an", "on", "under", "over", "between", "of", "and", "but", "or", "with", "upon"];
		return source
			.replace(/\s+/, " ")
			.split(" ")
			.map(o => o.toLowerCase())
			.map((o, index) => (index !== 0 && exceptions.includes(o) ? o : this.to_sentence_case(o)))
			.join(" ");
	}

	// -------------------------------------------------------------------------------------------------
	/** Generate a specified length of random string */
	generate_random_string(length: number = 32): string {
		const pool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		return this._chance.string({ pool, length });
	}

	// -------------------------------------------------------------------------------------------------
	/** Generate a random number */
	generate_random_number(min = 1, max = 1000): number {
		return this._chance.integer({ min, max });
	}

	// -------------------------------------------------------------------------------------------------
	/** Get file extension */
	get_file_extension(name: string): string {
		if (!!name) {
			const length = name.length;
			const index = name.lastIndexOf(".");
			return name.substr(index, length - 1);
		}
		return undefined;
	}

	// -------------------------------------------------------------------------------------------------
	/** Gets the base64 equivalent of a file */
	async get_base_64(path_to_file: string): Promise<string> {
		const readAsync = promisify(readFile);
		try {
			return await readAsync(path_to_file, "base64");
		} catch (error) {
			return Promise.reject();
		}
	}

	// -------------------------------------------------------------------------------------------------
	/** Sanitizes user inputted text */
	sanitize_string(source: string): string {
		if (this.is_null_or_undefined(source)) return undefined;
		return filterXSS(source);
	}

	// -------------------------------------------------------------------------------------------------
	/** Returns true if an entity is null or undefined */
	is_null_or_undefined(value: any): boolean {
		return typeof value === "undefined" || value == null;
	}

	// -------------------------------------------------------------------------------------------------
	/** Removes all properties that are undefined and returns a fresh clean object */
	purge_object<T>(source: T): T {
		if (this.is_null_or_undefined(source)) return undefined;

		const target = {} as T;

		Object.entries(source).forEach(([key, value]) => {
			if (!this.is_null_or_undefined(value)) {
				if (typeof value === "object") value = this.purge_object(value);
				target[key] = value;
			}
			return;
		});

		return target;
	}

	// -------------------------------------------------------------------------------------------------
	/** Skip and Take */
	get_paginated_items<T>(list: Array<T> = new Array<T>(), page: number = 1): Array<T> {
		const page_size = parseInt(process.env.PAGE_SIZE);
		const index = page_size * (page - 1);
		return list.slice(index, page_size * page);
	}

	// -------------------------------------------------------------------------------------------------
	/** Normalize order from url query */
	normalize_order<T>(source: new () => T, order: string): keyof T {
		order = this.sanitize_string(order);

		if (this.is_null_or_undefined(order)) return "created_at" as keyof T;

		const keys = Object.keys(new source());

		if (!keys.includes(order.toLowerCase())) return "created_at" as keyof T;

		return order.toLowerCase() as keyof T;
	}

	// -------------------------------------------------------------------------------------------------
	/** Normalize order direction from url query */
	normalize_order_direction(order_direction: string): "ASC" | "DESC" {
		const valid_order_directions = ["ASC", "DESC"];

		order_direction = this.sanitize_string(order_direction);

		if (!!order_direction && valid_order_directions.includes(order_direction.toUpperCase())) {
			return order_direction.toUpperCase() as any;
		}

		return "DESC";
	}

	// -------------------------------------------------------------------------------------------------
	/** Returns a version of the api response that includes the unknown error response */
	handle_unknown_errors(resp: Response, api_response: ApiResponse, error: any): ApiResponse {
		this._log_service.log_exception(error);
		api_response = Object.assign({}, api_response);
		api_response.errors = [new ApiResponseError({ message: Messages.unexpected_error })];
		resp.status(StatusCodes.internal_server_error);
		return api_response;
	}

	// -------------------------------------------------------------------------------------------------
	/** Returns a version of the api response that includes the not found error response */
	handle_not_found_error(resp: Response, api_response: ApiResponse) {
		api_response = Object.assign({}, api_response);
		api_response.errors = [new ApiResponseError({ message: Messages.entity_not_found })];
		resp.status(StatusCodes.not_found);
		return api_response;
	}
}
