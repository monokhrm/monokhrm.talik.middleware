import { Attachment } from "nodemailer/lib/mailer";

export type SendEmailConfigType = {
	to: string | Array<string>;
	subject?: string;
	body?: string;
	attachments?: Array<Attachment>;
};
