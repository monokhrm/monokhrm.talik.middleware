export type UploadedFileType = {
	name: string;
	imagePath: string;
	size: number;
	width: number;
	height: number;
	url: string;
};
