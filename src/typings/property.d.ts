import { PropertyTypeEnum } from "../enums/PropertyTypeEnum";

export type PropertyRequestType = {
	locality: string;
	type: PropertyTypeEnum;
	bedrooms: number;
	min_price: number;
	max_price: number;
	furnished: boolean;
	tags: string;
};
